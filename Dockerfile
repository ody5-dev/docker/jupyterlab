FROM debian:bookworm
MAINTAINER Jonathan Pieper <ody55eus@mailbox.org>
ENV NODE_MAJOR=20

RUN echo "deb-src http://deb.debian.org/debian bookworm main" >> /etc/apt/sources.list
RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y git wget ca-certificates curl gnupg texlive texlive-science texlive-latex-extra texlive-xetex dvipng man-db cm-super python3-pip graphviz && \
  rm -rf /var/lib/apt/lists/*

# Install NodeJS
RUN mkdir -p /etc/apt/keyrings && \
  curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
  echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
  apt-get update && apt-get install nodejs -y

RUN wget https://github.com/jgm/pandoc/releases/download/3.1.7/pandoc-3.1.7-1-amd64.deb && \
  dpkg -i pandoc-3.1.7-1-amd64.deb && \
  rm -f pandoc-3.1.7-1-amd64.deb

RUN python3 -m pip install --upgrade --break-system-packages pip && \
  python3 -m pip install --upgrade --break-system-packages \
    jupyterlab \
    ipywidgets \
    jupyterlab_latex \
    plotly \
    sphinx \
    recommonmark \
    nbsphinx  \
    sphinx_rtd_theme \
    bokeh \
    numpy \
    scipy \
    numexpr \
    patsy \
    scikit-learn \
    scikit-image \
    matplotlib \
    ipython \
    pandas \
    sympy \
    seaborn \
    nose \
    ipytest \
    coverage \
    h5py \
    lolviz \
    graphviz \
    ipympl

# Install Science Plots
RUN python3 -m pip install --break-system-packages git+https://github.com/garrettj403/SciencePlots.git

COPY bin/entrypoint.sh /usr/local/bin/
COPY config/ /root/.jupyter/

EXPOSE 8888
VOLUME /notebooks
WORKDIR /notebooks
